# Changelog

## 0.1.1 - Minor tweaks

Some minor bugfixes, wording, and localization tweaks

### Bugfixes

* Added checking for a combat being present before attempting to inject buttons for changing modes
* Fixed CSS rules to properly size and center the action icon

## 0.1.0 - Initial relase

Initial release of the module

### Features 

* Adds a button to the combat tracker to switch the tracker to OOCT mode
* Adds a configuration window in settings to define any number of actions that users can pick from

## 