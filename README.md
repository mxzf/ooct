# Out of Combat Tracker

This module adds a button in the combat tracker which can be used to switch the combat to another mode where initiative isn't displayed and instead a user-chosen icon is shown to indicate what action (from the GM-defined list) that character is taking.  


## Installation

Module manifest: https://gitlab.com/mxzf/ooct/-/releases/permalink/latest/downloads/module.json

## Usage

First, you can define a list of actions that characters can take when a combat is set to OOCT mode via a settings window

![settings image](documentation/ooct_settings.webp)

After creating a Combat Encounter, you can click the button in the header to switch the combat to OOCT mode.  At which point the initiative column is replaced by icons for the various actions as selected by the users for various characters by clicking on the combatant to get a drop-down list of options.  Advancing to the next round will reset all combatants to the first action defined (generally an "Undecided" option).

![tracker image](documentation/ooct_tracker.webp)