function iconToHTML({icon,label}={}){ return `<i class="fa-solid ${icon}" data-tooltip="${label}"></i>`}

function configIconUpdate(ev) {ev.target.parentElement.querySelector('i:first-of-type').className = `fa-solid ${ev.target.value}`}
function configTextUpdate(ev) {ev.target.parentElement.querySelector('i:first-of-type').setAttribute('data-tooltip',ev.target.value)}

class OOCTConfig extends FormApplication {
    static get defaultOptions() {
        const options = super.defaultOptions;
        options.id = "ooct-settings";
        options.template = "modules/ooct/templates/settings.hbs";
        options.height = "auto";
        options.width = 550;
        options.title = "Out of Combat Tracker Actions";
        return options;
    }

    getData(options={}) {
        let context = super.getData()
        context.choices = game.settings.get('ooct','choices').map(({label,icon}) => ({label,icon,rendered:iconToHTML({icon,label})}))
        return context
    }

    activateListeners(html) {
        super.activateListeners(html);
        // Handle swapping out the icon on-change
        html.find('li input:nth-of-type(2)').change(configIconUpdate)
        // Handle swapping out the hover text on-change
        html.find('li input:nth-of-type(1)').change(configTextUpdate)
        // Handle deleting the row
        html.find('li a .fa-trash').click(ev => ev.target.parentElement.parentElement.remove())
        // Handle resetting the window
        html.find('#reset-actions').click(() => this.render(true)) // Only kinda partially working
        html.find('.add-row').click(ev => {
            let currentcount = html[0].querySelectorAll(".action").length
            let new_row = $(`<li class="action form-group"><i class="fa-solid fa-question"></i><input name="choices.${currentcount}.label" class="label-text" type="text"/><input name="choices.${currentcount}.icon" class="icon-text" type="text"/><a><i class="fa-solid fa-trash" data-tooltip="Delete"></i></a></li>`)
            new_row.insertBefore(ev.currentTarget)
            new_row.find('.icon-text').change(configIconUpdate)
            html.find('.label-text').change(configTextUpdate)
            new_row.find('a .fa-trash').click(ev => ev.target.parentElement.parentElement.remove())
        })
    }

    async _updateObject(event, formData) {
        game.settings.set('ooct','choices',
            Object.values(expandObject(formData)?.choices ?? {0:{'label':'Undecided', 'icon': 'fa-question'}}).filter(c => c.icon&&c.label)
        )
    }
}

Hooks.on('init', () => {
    game.settings.register('ooct', 'choices', {
        scope: 'world',
        config:false,
        type: Array,
        default: [
            {'label':'Undecided', 'icon': 'fa-question'},
            {'label':'Searching', 'icon':'fa-magnifying-glass'},
        ],
        onChange: () => ui.combat.render()
    })

    game.settings.registerMenu("ooct", "choice_config", {
      name: "OOCT.settings.config.title",
      label: "OOCT.settings.config.label",
      hint: "OOCT.settings.config.hint",
      icon: "fas fa-list-radio",
      type: OOCTConfig,
      restricted: false
    });
})

Hooks.on('renderCombatTracker', (app,html,context) => {
    if (!app?.viewed) return // Skip entirely if there's no currently viewed combat
    // Insert toggle button into header
    if (game.user.isGM){
        let mode_button = `<a class="combat-button" data-tooltip="OOCT.toggle_label" data-control="ooct" ${app.viewed.getFlag('ooct','exploration_mode') ? 'style="color:var(--color-shadow-primary);"' : ''}><i class="fas fa-shoe-prints"></i></a>`
        html.find('.combat-button.combat-control[data-control="rollNPC"]').after(mode_button)
        html.find('.combat-button[data-control="ooct"]').click(ev => app.viewed.setFlag('ooct','exploration_mode',!app.viewed?.flags?.ooct?.exploration_mode))
    }

    // Skip the actual rendering changes if the current combat isn't toggled to exploration mode
    if (!app.viewed.getFlag('ooct','exploration_mode')) return

    html.find('.token-initiative').each((i,el) => {
        let combatant = app.viewed.combatants.get(el.parentElement.dataset.combatantId)
        el.innerHTML = iconToHTML(combatant.getFlag('ooct','action') ?? game.settings.get('ooct','choices')[0])
        //el.style.cssText = 'font-size:2em' // move to custom style
    })
    
    // Register the context menu for picking stuff
    new ContextMenu(html, ".directory-item",  game.settings.get('ooct','choices').map(i => ({
        name: i.label, 
        icon: iconToHTML(i), 
        condition: li => app.viewed.combatants.get(li.data("combatant-id")).isOwner,
        callback: li => app.viewed.combatants.get(li.data("combatant-id")).setFlag('ooct','action', i)
    })), {eventName:'click'});
})

// Reset the selected actions at the start of each round
Hooks.on('combatRound', (combat, updateData, updateOptions) => {
    if (!combat.getFlag('ooct','exploration_mode')) return
    combat.updateEmbeddedDocuments('Combatant', combat.combatants.map(c => ({_id:c.id,"flags.ooct.action":null})))

})